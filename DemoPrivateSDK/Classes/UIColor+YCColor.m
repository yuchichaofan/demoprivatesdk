//
//  UIColor+YCColor.m
//  DemoPrivateSDK
//
//  Created by mtime on 2020/3/26.
//

#import "UIColor+YCColor.h"


@implementation UIColor (YCColor)

+ (UIColor *)randomColor{
    return UIColor.redColor;
}

+ (UIImage *)getPiC{
    //获取图片
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL * bundle_URL = [bundle URLForResource:@"DemoPrivateSDK" withExtension:@"bundle"];
    UIImage * image = [UIImage imageNamed:@"map.jpeg"
                                 inBundle:[NSBundle bundleWithURL:bundle_URL]
            compatibleWithTraitCollection:nil];
    return image;
}

@end
