//
//  UIColor+YCColor.h
//  DemoPrivateSDK
//
//  Created by mtime on 2020/3/26.
//

#if TARGET_OS_MACCATALYST
#import <AppKit/AppKit.h>
#endif

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (YCColor)

+ (UIColor *)randomColor;

+ (UIImage *)getPiC;

@end

NS_ASSUME_NONNULL_END
