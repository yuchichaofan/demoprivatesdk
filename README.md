# DemoPrivateSDK

[![CI Status](https://img.shields.io/travis/shao wenxue/DemoPrivateSDK.svg?style=flat)](https://travis-ci.org/shao wenxue/DemoPrivateSDK)
[![Version](https://img.shields.io/cocoapods/v/DemoPrivateSDK.svg?style=flat)](https://cocoapods.org/pods/DemoPrivateSDK)
[![License](https://img.shields.io/cocoapods/l/DemoPrivateSDK.svg?style=flat)](https://cocoapods.org/pods/DemoPrivateSDK)
[![Platform](https://img.shields.io/cocoapods/p/DemoPrivateSDK.svg?style=flat)](https://cocoapods.org/pods/DemoPrivateSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DemoPrivateSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DemoPrivateSDK'
```

## Author

shao wenxue, 362907080@qq.com

## License

DemoPrivateSDK is available under the MIT license. See the LICENSE file for more info.
