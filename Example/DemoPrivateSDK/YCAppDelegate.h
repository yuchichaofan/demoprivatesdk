//
//  YCAppDelegate.h
//  DemoPrivateSDK
//
//  Created by shao wenxue on 03/26/2020.
//  Copyright (c) 2020 shao wenxue. All rights reserved.
//

@import UIKit;

@interface YCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
