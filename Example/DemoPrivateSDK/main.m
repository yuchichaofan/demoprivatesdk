//
//  main.m
//  DemoPrivateSDK
//
//  Created by shao wenxue on 03/26/2020.
//  Copyright (c) 2020 shao wenxue. All rights reserved.
//

@import UIKit;
#import "YCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YCAppDelegate class]));
    }
}
