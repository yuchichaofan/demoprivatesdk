#
# Be sure to run `pod lib lint DemoPrivateSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DemoPrivateSDK'
  s.version          = '0.1.0'
  s.summary          = 'A short description of DemoPrivateSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  #s.homepage         = 'https://github.com/shao wenxue/DemoPrivateSDK'
  s.homepage         = 'https://gitlab.com/dashboard/projects'

  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'shao wenxue' => '362907080@qq.com' }
  s.source           = { :git => 'https://gitlab.com/yuchichaofan/demoprivatesdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'DemoPrivateSDK/Classes/**/*'
  s.resource_bundles = {
     'DemoPrivateSDK' => ['DemoPrivateSDK/Assets/*.bundle']
  }
  s.resource = "DemoPrivateSDK/Assets/*.bundle"
# s.resource_bundles = {
  #   'DemoPrivateSDK' => ['DemoPrivateSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
